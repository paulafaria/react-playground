import React from 'react';

import SearchBar from './search-bar';
import UserInfo from './user-info';
import Actions from './actions.js';

const AppContent = ({
  userinfo,
  handleSearch,
  getRepos,
  repos }) => {
  return (
    <div>
      <SearchBar handleSearch={handleSearch} />
      {!!userinfo && <UserInfo userinfo={userinfo} />}
      <Actions getRepos={getRepos} />
    </div>
  );
};

export default AppContent;
