import React from 'react';

const UserInfo = ({ userinfo }) => {
  return (
    <div className='user-info'>
      <img src={userinfo.image} alt='user image' className='user-photo' width='50' />
      <p className='username'>
        <a href='https://github.com/paulahfaria'>{userinfo.username}</a>
      </p>
      <ul>
        <li>
          followers: {userinfo.followers}
        </li>
        <li>
          following: {userinfo.following}
        </li>
        {userinfo.email && <li>email: {userinfo.email}</li> }

        {userinfo.bio && <li>bio: {userinfo.bio}</li> }

      </ul>
    </div>
  );
};

export default UserInfo;
