import React from 'react';

const Searchbar = ({ handleSearch }) => {
  return (
    <div className='search-bar'>
      <input type='text' onKeyUp={handleSearch} />
    </div>
  );
};

export default Searchbar;
