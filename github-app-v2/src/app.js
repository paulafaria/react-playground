'use strict';

import React, { Component } from 'react';
import AppContent from './components/app-content';
import ajax from '@fdaciuk/ajax';

class App extends Component {
  constructor () {
    super();

    this.state = {
      userinfo: null,
      repos: [],
      starred: []
    };
  }

  handleSearch (e) {
    const value = e.target.value;
    const ENTER = 13;
    const key = e.wich || e.keyCode;
    if (key === ENTER) {
      ajax().get(`https://api.github.com/users/${value}`)
      .then((result) => {
        this.setState({
          userinfo: {
            image: result.avatar_url,
            username: result.name,
            followers: result.followers,
            following: result.following,
            email: result.email,
            bio: result.bio
          },
          repos: [],
          starred: []
        });
      });
    }
  }

  getRepos (type) {
    return (e) => {
      const url = `https://api.github.com/users/paulahfaria/${type}`;
      ajax().get(url)
      .then((result) => {
        console.log(result)
        this.setState({
          [type]: result.map((repo) => ({
            name: repo.name,
            link: repo.html_url
          }))
        });
      });
    };
  }

  render () {
    return <AppContent
      userinfo={this.state.userinfo}
      handleSearch={(e) => this.handleSearch(e)}
      getRepos={this.getRepos('repos')}
      getStarred={this.getRepos('starred')}
      repos={this.state.repos}
      starred={this.state.starred}
      />;
  }
}

export default App;
