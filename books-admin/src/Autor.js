import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './components/InputCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';


class FormularioAutor extends Component{
    constructor(){
        super();
        this.state = {
            nome:'',
            email:'',
            senha:''
            };
      }

      salvaAlteracao(nomeInput, evento){
        var campo = {};
        campo[nomeInput] = evento.target.value;
        this.setState(campo);
      }
    
    
      enviaForm = evento =>{
        evento.preventDefault();
    
        $.ajax({
          url: 'http://cdc-react.herokuapp.com/api/autores',
          contentType: 'application/json',
          dataType: 'json',
          type:'post',
          data: JSON.stringify({nome:this.state.nome, email:this.state.email, senha:this.state.senha}),
          success: function(resposta){
            PubSub.publish('atualiza-lista-autores', resposta);
            this.setState({nome: '', email: '', senha: ''})
            console.log(this.state.success);
          }.bind(this),
          error: function(resposta){
            console.log('ops nao deu')
            if(resposta.status === 400){
              new TratadorErros().publicaErros(resposta.responseJSON);
            }
          },
          beforeSend: function(){
            PubSub.publish("limpa-erros", {});
          }
        })
      }

    render(){
        return(
            <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" onSubmit={this.enviaForm} method="post">
                    
                    <InputCustomizado id="nome" type="text" name="nome" value={this.state.nome} onChange={this.salvaAlteracao.bind(this, 'nome')} label="Nome:"/>
                    <InputCustomizado id="email" type="email" name="email" value={this.state.email} onChange={this.salvaAlteracao.bind(this, 'email')} label="E-mail:"/>
                    <InputCustomizado id="senha" type="password" name="senha" value={this.state.senha} onChange={this.salvaAlteracao.bind(this, 'senha')} label="Senha:"/>
                    
                    <div className="pure-control-group">                                  
                    <label></label> 
                    <button type="submit" className="pure-button pure-button-primary">Gravar</button>                                    
                    </div>
                </form>             
            </div>  
        )
    }
}

class TabelaAutores extends Component{
    render(){
        return(
            <div>         
                <table className="pure-table" width="500px">
                  <thead>
                    <tr>
                      <th>Nome</th>
                      <th>email</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* <tr>
                      <td>Alberto</td>                
                      <td>alberto.souza@caelum.com.br</td>                
                    </tr> */}
                    {
                      this.props.lista.map((autor)=>{
                        return (
                          <tr width="500px" key={autor.id}>
                            <td width="250">{autor.nome}</td>                
                            <td width="250">{autor.email}</td>         
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table> 
            </div>    
        );
    }
}

class AutorBox extends Component{
    constructor(){
        super();
        this.state = {
            lista: []
            };
      }
    
    componentDidMount(){
    const url = 'http://cdc-react.herokuapp.com/api/autores';

    fetch(url)
        .then(response =>{
        return response.json()
        })
        .then(data => {
        this.setState({lista:data})
        })

    PubSub.subscribe('atualiza-lista-autores', function(topico, novaLista){
      this.setState({lista:novaLista})
    }.bind(this))
        
    }

    render(){
        return(
            <div>
              <div className="header">
                <h1>Cadastro de Autores</h1>
                <h2>A subtitle for your page goes here</h2>
              </div>
              <div className="content" id="content">
                <FormularioAutor/>            
                <TabelaAutores lista={this.state.lista}/> 
              </div>
            </div>
        )
    }
}

export default AutorBox;