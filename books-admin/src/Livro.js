import React, { Component } from 'react';
import $ from 'jquery';
import InputCustomizado from './components/InputCustomizado';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';


class FormularioLivro extends Component{
    constructor(){
        super();
        this.state = {
            titulo:'',
            preco:'',
            autorId:''
            };
      }

      setTitulo = evento =>{
        this.setState({titulo:evento.target.value})
      }
      setPreco = evento =>{
        this.setState({preco:evento.target.value})
      }
      setAutorId = evento =>{
        this.setState({autorId:evento.target.value})
      }
    
    
      enviaForm = evento =>{
        evento.preventDefault();
        console.log('enviando dados');
        console.log(this.state.titulo);
        console.log(this.state.preco);
        console.log(this.state.autorId);
    
        $.ajax({
          url: 'http://cdc-react.herokuapp.com/api/livros',
          contentType: 'application/json',
          dataType: 'json',
          type:'post',
          data: JSON.stringify({titulo:this.state.titulo, preco:this.state.preco, autorId:this.state.autorId}),
          success: function(resposta){
            PubSub.publish('atualiza-lista-livros', resposta);
            this.setState({titulo: '', preco: '', autorId: ''})
          }.bind(this),
          error: function(resposta){
            console.log('ops nao deu')
            if(resposta.status === 400){
              new TratadorErros().publicaErros(resposta.responseJSON);
            }
          },
          beforeSend: function(){
            PubSub.publish("limpa-erros", {});
          }
        })
      }

    render(){
        return(
            <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" onSubmit={this.enviaForm} method="post">
                    
                    <InputCustomizado id="titulo" type="text" name="titulo" value={this.state.titulo} onChange={this.setTitulo} label="Titulo:"/>
                    <InputCustomizado id="preco" type="text" name="preco" value={this.state.preco} onChange={this.setPreco} label="Preço:"/>
                    
                    <div className="pure-control-group">
                        <label htmlFor="autorId">Selecione:</label> 
                        <select name="autorId" onChange={this.setAutorId} id="autorId" value={this.state.autorId}>
                            <option value="">Selecione o autor</option>
                            {   
                                this.props.autores.map(function(autor){
                                    return(
                                        <option value={autor.id} key={autor.id}>{autor.nome}</option>
                                    )
                                })
                            }
                        </select>
                    </div>
                    
                    <div className="pure-control-group">                                  
                    <label></label> 
                    <button type="submit" className="pure-button pure-button-primary">Gravar</button>                                    
                    </div>
                </form>             
            </div>  
        )
    }
}

class TabelaLivros extends Component{
    render(){
        return(
            <div>         
                <table className="pure-table" width="500px">
                  <thead>
                    <tr>
                      <th>Título</th>
                      <th>Preço</th>
                      <th>Autor</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.lista.map((livro)=>{
                        return (
                            <tr width="500px" key={livro.id}>
                              <td width="250">{livro.titulo}</td>                
                              <td width="250">{livro.preco}</td>
                              <td width="250">{livro.autor.nome}</td>
                            </tr>
                          )
                      })
                    }
                  </tbody>
                </table> 
            </div>    
        );
    }
}

class LivroBox extends Component{
    constructor(){
        super();
        this.state = {
            lista: [],
            autores: []
            };
      }

      componentDidMount(){
        const urlLivros = 'http://cdc-react.herokuapp.com/api/livros';
    
        fetch(urlLivros)
            .then(response =>{
            return response.json()
            })
            .then(data => {
            this.setState({lista:data})
            })
    
        PubSub.subscribe('atualiza-lista-livros', function(topico, novaLista){
          this.setState({lista:novaLista})
        }.bind(this))

        const urlAutores = 'http://cdc-react.herokuapp.com/api/autores';

        fetch(urlAutores)
            .then(response =>{
            return response.json()
            })
            .then(data => {
            this.setState({autores:data})
            })
    
        PubSub.subscribe('atualiza-lista-autores', function(topico, novaLista){
          this.setState({autores:novaLista})
        }.bind(this))
            
        }

    render(){
        return(
            <div>
              <div className="header">
                <h1>Cadastro de Livros</h1>
                <h2>A subtitle for your page goes here</h2>
              </div> 
              <div className="content" id="content">
                <FormularioLivro autores={this.state.autores}/>
                <TabelaLivros lista={this.state.lista}/> 
              </div>
            </div>
        )
    }
}


export default LivroBox;