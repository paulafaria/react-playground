import { Component } from 'react';
import {browserHistory} from 'react-router';

class Logout extends Component{
    componentWillMount(){
        localStorage.removeItem('auth-token');
        browserHistory.push('/');
    }
    render(){
        return null;
    }
}

export default Logout;