import React, { Component } from 'react';
import {Link} from 'react-router';
import Pubsub from 'pubsub-js';


class FotoHeader extends Component{
    render(){
        return(
            <header className="foto-header">
                <figure className="foto-usuario">
                    <img src={this.props.fotoHeader.urlPerfil} alt={this.props.fotoHeader.loginUsuario}/>
                    <figcaption className="foto-usuario">
                    <Link to={`/timeline/${this.props.fotoHeader.loginUsuario}`}>
                        {this.props.fotoHeader.loginUsuario}
                    </Link>
                    </figcaption>
                </figure>
                <time className="foto-data">{this.props.fotoHeader.horario}</time>
            </header>
        );
    }
}

class FotoImage extends Component{
    render(){
        return <img alt="foto" className="foto-src" src={this.props.fotoImage.urlFoto}/>
    }
}
class FotoInfo extends Component{

    constructor(props){
        super(props);
        this.state ={
            likers: this.props.fotoInfo.likers
        }
    }

    componentWillMount(){
      Pubsub.subscribe('atualiza-liker', (topico, infoLiker) => {
          console.log(infoLiker)
      })
    }
    
    render(){
        return(
            <div className="foto-info">
                <div className="foto-info-likes">
                    {
                        this.state.likers.map(liker => {
                            return(
                                <Link to={`/timeline/${liker.login}`} key={liker.login}>{liker.login}, </Link>
                            )
                        })
                    }
                    curtiram
                </div>

                <p className="foto-info-legenda">
                    <a className="foto-info-autor">{this.props.fotoInfo.loginUsuario} </a>
                    {this.props.fotoInfo.comentario}
                </p>

                <ul className="foto-info-comentarios">
                    {
                        this.props.fotoInfo.comentarios.map(comentario =>{
                            return(
                                <li className="comentario" key={comentario.id}>
                                    <Link to={`/timeline/${comentario.login}`} className="foto-info-autor">
                                        {comentario.login} 
                                    </Link>
                                    <span> {comentario.texto}</span>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        );
    }
}

class FotoAtualizacoes extends Component{
    constructor(props){
        super(props);
        this.state ={
            likeada: this.props.fotoAtualizacoes.likeada
        }
    }
    like = event =>{
        event.preventDefault();
        const requestInfo = {
            method: 'POST'
        }
        fetch(`https://instalura-api.herokuapp.com/api/fotos/${this.props.fotoAtualizacoes.id}/like?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, requestInfo)
        .then(response =>{
            if(response.ok){
                return response.json();
            }else{
                throw new Error("nao foi possivel realizar o like");
            }
        })
        .then(liker=>{
            this.setState({
                likeada: !this.state.likeada
            })
            Pubsub.publish('atualiza-liker', {fotoId: this.props.fotoAtualizacoes.id, liker})
        })
        
    }
    render(){
        return(
            <section className="fotoAtualizacoes">
                <a onClick={this.like} className={this.state.likeada ? 'fotoAtualizacoes-like-ativo' : 'fotoAtualizacoes-like'}>Likar</a>
                <form className="fotoAtualizacoes-form">
                    <input type="text" placeholder="Adicione um comentário..." className="fotoAtualizacoes-form-campo"/>
                    <input type="submit" value="Comentar!" className="fotoAtualizacoes-form-submit"/>
                </form>
            </section>
        );
    }
}
class FotoItem extends Component{
    render(){
        return(
            <div className="foto">
                    <FotoHeader fotoHeader={this.props.foto}/>
                    <FotoImage fotoImage={this.props.foto}/>
                    <FotoInfo fotoInfo={this.props.foto}/>
                    <FotoAtualizacoes fotoAtualizacoes={this.props.foto}/>
                </div> 
        );
    }
}

export default FotoItem;