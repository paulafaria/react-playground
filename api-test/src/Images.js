import React, { Component } from 'react';


class Images extends Component{
    render(){
        return(
            <img src={this.props.foto.images.thumbnail.url} alt=""/>
        )
    }
}

export default Images;