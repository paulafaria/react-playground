import React, { Component } from 'react';
import Images from './Images';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      images: []
    }
  }
  
  componentWillMount() {
    const api_token = '1238779.4ff4908.b294fe5166604efe9b3f8c4289276723';
    const api_url = `https://api.instagram.com/v1/users/self/media/recent/?access_token=${api_token}&count=19`


    fetch(api_url)
    .then(response => response.json())
    .then(data => {
      this.setState({images: data.data})
      console.log(this.state.images)
    })
    .catch( e => console.log('error', e))
  }
  
  render() {
    return (
      <div className="App">
      <p>primeira parte</p>
        {
          this.state.images.slice(0,7).map((image)=>{
            return(
              <Images key={image.id} foto={image}/>
            )
          })
        }
        <p>segunda parte</p>
        {
          this.state.images.slice(7,19).map((image)=>{
            return(
              <Images key={image.id} foto={image}/>
            )
          })
        }
      </div>
    );
  }
}

export default App;
