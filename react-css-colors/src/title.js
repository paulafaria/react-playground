'use strict';

import React from 'react';

const Title = ({name}) => (
    <h1>oi {name}</h1>
)

Title.defaultProps = {
    name: 'zzzzzz'
}
// const Title = React.createClass({
//     getDefaultProps: function(){
//         return{
//             name: 'nofirstname',
//             lastName: 'nolastname'
//         }
//     },               
//     render: function(){
//         return(
//             <h1>Olar {`${this.props.name} ${this.props.lastName.first}${this.props.lastName.last}`}</h1>
//         )
//     }
// })

export default Title;