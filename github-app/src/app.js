'use strict';

import React, { Component } from 'react';
import AppContent from './components/app-content';
import ajax from '@fdaciuk/ajax';

class App extends Component {
  constructor () {
    super();
    this.state = {
      userInfo: null,
      repos: [],
      starred: []
    };
  }

  getGitHubApiUrl (username, type) {
    return `http://api.github.com/users/${username}${type}`;
  }
  handleSearch (e) {
    const value = e.target.value;
    const keyCode = e.which || e.keyCode;
    const ENTER = 13;
    if (keyCode === ENTER) {
      ajax().get(`http://api.github.com/users/${value}`)
      .then((result) => {
        this.setState({
          userInfo: {
            username: result.name,
            photo: result.avatar_url,
            login: result.login,
            repos: result.public_repos,
            followers: result.followers,
            following: result.following
          },
          repos: [],
          starred: []
        });
      });
    }
  }
  getRepos (type) {
    return (e) => {
      const url = `https://api.github.com/users/${this.state.userInfo.login}/${type}`;
      console.log(url);
      ajax().get(url)
      .then((result) => {
        this.setState({
          [type]: result.map((repo) => ({
            name: repo.name,
            link: repo.html_url
          }))
        });
      });
    };
  }
  render () {
    return <AppContent
      userinfo={this.state.userInfo}
      repos={this.state.repos}
      starred={this.state.starred}
      handleSearch={(e) => this.handleSearch(e)}
      getRepos={this.getRepos('repos')}
      getStarred={this.getRepos('starred')}
      />;
  }
}

export default App;
