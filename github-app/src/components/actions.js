'use strict';

import React from 'react';

const Actions = ({ getRepos, getStarred }) => (
  <div className='actions'>
    <button onClick={getRepos}>ver repositórios</button>
    <button onClick={getStarred}>ver favoritos</button>
  </div>
);

export default Actions;
