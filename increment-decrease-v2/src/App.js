import React, { Component } from 'react';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      counter: 0
    }
  }
  Increase = () =>{
    this.setState({counter: this.state.counter + 1})
  }
  Decrease = () =>{
    this.setState({counter: this.state.counter - 1})
  }
  render() {
    return (
      <div>
        <button onClick={this.Decrease}>-1</button>
        <button onClick={this.Increase}>+1</button>
        <p>{this.state.counter}</p>
      </div>
    );
  }
}

export default App;
