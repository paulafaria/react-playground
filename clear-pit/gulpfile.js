var gulp = require('gulp'),
    changed = require('gulp-changed'),
    sass = require('gulp-sass'),
    imagemin = require('imagemin'),
    imageminPngquant = require('imagemin-pngquant'),
    webserver = require('gulp-webserver'),
    filter = require('gulp-filter'),
    // jshintstylish = require('jshint-stylish'),
    hint = require('gulp-jshint');


gulp.task('sass', function () {
    return gulp.src('app/assets/styles/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('app/assets/styles'));
    });
    
    gulp.task('sass:watch', function () {
    gulp.watch('app/assets/styles/**/*.scss', ['sass']);
});


gulp.task('server', function(){
    gulp.src('app').pipe(webserver({ 
        livereload: true, 
        open: true
    }))
})

gulp.task('hint', function(){
    gulp.src('app/assets/scripts/**/*.js')
    .pipe(filter(function(file){
        var fn = file.path.split('/')[file.path.split('/').length-1];
        return fn !== "bundle.js";
    }))
    .pipe(hint())
    .pipe(hint.reporter('jshint-stylish'));
})




gulp.task('default', ['sass', 'hint' ,'server']);
