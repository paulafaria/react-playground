'use strict';

import React, {Component} from 'react';

class App extends Component {
  constructor () {
    super();
    this.state = {
      checked: false,
      showContent: false
    };
  }
  render () {
    return (
      <div>
        <label>
          <input type='checkbox' checked={this.state.checked} onChange={(e) => {
            this.setState({
              checked: !this.state.checked
            }, () => {
              this.setState({
                showContent: this.state.checked
              })
            });
          }} />
          just a simple checkbox, but made w react hum
        </label>
        {this.state.showContent ? <div>here</div> : ''}
      </div>
    );
  }
}

export default App;
