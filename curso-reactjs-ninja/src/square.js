import React from 'react';

const Square = ({color, colorName}) => {
  return (
    <div style={{
      height: '100px',
      width: '100px',
      background: color,
      display: 'inline-block',
      margin: '5px',
      position: 'relative'
    }}>
      <span style={{
        display: 'block',
        position: 'absolute',
        bottom: 0,
        textAlign: 'center',
        width: '100%'
      }}>{colorName}</span>
    </div>
  );
};

Square.defaultProps = {
  color: 'red'
};

export default Square;
