import React, {Component} from 'react';

class Timer extends Component {
  constructor () {
    super();
    this.state = {
      time: 0
    };
    this.timer;
    console.log('constructor timer');
  }
  componentDidMount () {
    this.timer = setInterval(() => {
      this.setState({time: this.state.time + 1});
    }, 1000);
    console.log('componentDidMount timer');
  }
  componentWillUnmount () {
    clearInterval(this.timer);
  }
  componentWillReceiveProps (nextProps) {
    console.log('componentWillReceiveProps timer', this.props, nextProps);
  }
  shouldComponentUpdate (nextProps, nextState) {
    // console.log('shouldComponentUpdate timer', this.props, nextProps);
    return this.props.time !== nextProps.time;
  }
  componentWillUpdate (nextProps, nextState) {
    console.log('componentWillUpdate timer', this.props, nextProps);
  }
  componentDidUpdate (prevProps, prevState) {
    console.log('componentDidUpdate timer', this.props, prevProps);
  }

  render () {
    console.log('render time');
    return (
      <p>{this.state.time}</p>
    );
  }
}

export default Timer;
